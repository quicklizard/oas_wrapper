package oas_wrapper

import (
	"encoding/json"
	"fmt"
	"github.com/go-courier/oas"
	"reflect"
	"strconv"
	"strings"
)

var structTags = []string{"oas-example", "oas-desc", "oas-validation", "oas-format", "oas-default", "oas-permissions"}

type Object struct {
	api *oas.OpenAPI
}

func newObject(api *oas.OpenAPI) *Object {
	return &Object{
		api: api,
	}
}

func (s *Object) Define(v interface{}) (string, *oas.Schema) {
	var t reflect.Type
	var schema *oas.Schema
	var name string
	switch value := v.(type) {
	case reflect.Type:
		t = value
	default:
		t = reflect.TypeOf(v)
	}
	switch t.Kind() {
	case reflect.Struct:
		name, schema = s.resolveStruct(t)
	case reflect.Slice, reflect.Ptr:
		item := t.Elem()
		if item.Kind() == reflect.Struct {
			var singular string
			singular, schema = s.resolveSlice(item)
			name = fmt.Sprintf("%ss", singular)
		} else {
			schema = s.resolveField(t, nil)
		}
	case reflect.Map:
		key := s.resolveField(t.Key(), nil)
		value := s.resolveField(t.Elem(), nil)
		schema = oas.KeyValueOf(key, value)
	default:
		schema = s.resolveField(t, nil)
	}
	return name, schema
}

func (s *Object) resolveField(field reflect.Type, tag *reflect.StructTag) *oas.Schema {
	var b byte
	var byteKind = reflect.TypeOf(b).Kind()
	var schema *oas.Schema
	switch field.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Uint8, reflect.Uint16, reflect.Uint32:
		schema = oas.Integer()
	case reflect.Int64, reflect.Uint64:
		schema = oas.Long()
	case reflect.Float32:
		schema = oas.Float()
	case reflect.Float64:
		schema = oas.Double()
	case reflect.Bool:
		schema = oas.Boolean()
	case reflect.String:
		schema = oas.String()
	case reflect.Slice:
		_, schema = s.resolveSlice(field.Elem())
	case reflect.Map:
		key := s.resolveField(field.Key(), nil)
		value := s.resolveField(field.Elem(), nil)
		schema = oas.KeyValueOf(key, value)
	case reflect.Ptr:
		schema = s.resolveField(field.Elem(), nil)
		schema.Nullable = true
	case reflect.Struct:
		_, schema = s.resolveStruct(field)
	case byteKind:
		schema = oas.Byte()
	}
	if tag != nil {
		s.enrichSchema(tag, schema)
	}
	return schema
}

func (s *Object) validation(spec string) *oas.SchemaValidation {
	var validation = &oas.SchemaValidation{}
	var parts = strings.Split(spec, ";")
	for _, part := range parts {
		pair := strings.Split(part, ":")
		kind := pair[0]
		value := pair[1]
		switch kind {
		case "multipleOf":
			if f, err := strconv.ParseFloat(value, 64); err == nil {
				validation.MultipleOf = &f
			}
		case "maximum":
			if f, err := strconv.ParseFloat(value, 64); err == nil {
				validation.Maximum = &f
			}
		case "exclusiveMaximum":
			if f, err := strconv.ParseBool(value); err == nil {
				validation.ExclusiveMaximum = f
			}
		case "minimum":
			if f, err := strconv.ParseFloat(value, 64); err == nil {
				validation.Minimum = &f
			}
		case "exclusiveMinimum":
			if f, err := strconv.ParseBool(value); err == nil {
				validation.ExclusiveMinimum = f
			}
		case "maxLength":
			if f, err := strconv.ParseUint(value, 10, 64); err == nil {
				validation.MaxLength = &f
			}
		case "minLength":
			if f, err := strconv.ParseUint(value, 10, 64); err == nil {
				validation.MinLength = &f
			}
		case "pattern":
			validation.Pattern = value
		case "maxItems":
			if f, err := strconv.ParseUint(value, 10, 64); err == nil {
				validation.MaxItems = &f
			}
		case "minItems":
			if f, err := strconv.ParseUint(value, 10, 64); err == nil {
				validation.MinItems = &f
			}
		case "uniqueItems":
			if f, err := strconv.ParseBool(value); err == nil {
				validation.UniqueItems = f
			}
		case "maxProperties":
			if f, err := strconv.ParseUint(value, 10, 64); err == nil {
				validation.MaxProperties = &f
			}
		case "minProperties":
			if f, err := strconv.ParseUint(value, 10, 64); err == nil {
				validation.MinProperties = &f
			}
		case "enum":
			var enum []interface{}
			if err := json.Unmarshal([]byte(value), &enum); err == nil {
				validation.Enum = enum
			}
		}
	}
	return validation
}

func (s *Object) resolveSlice(o reflect.Type) (string, *oas.Schema) {
	if o.Kind() == reflect.Struct {
		name, schema := s.resolveStruct(o)
		plural := fmt.Sprintf("%ss", name)
		items := oas.ItemsOf(schema)
		s.api.AddSchema(plural, items)
		return name, s.api.RefSchema(plural)
	} else {
		schema := s.resolveField(o, nil)
		return "", oas.ItemsOf(schema)
	}
}

func (s *Object) resolveStruct(o reflect.Type) (string, *oas.Schema) {
	switch o.Name() {
	case "Time":
		name := o.Name()
		schema := oas.DateTime()
		s.api.AddSchema(name, schema)
		return name, schema
	default:
		return s.resolveCustomStruct(o)
	}
}

func (s *Object) resolveCustomStruct(o reflect.Type) (string, *oas.Schema) {
	var object = make(oas.Props, 0)
	var required = make([]string, 0)
	for i := 0; i < o.NumField(); i++ {
		field := o.Field(i)
		// skip unexported fields
		if strings.ToLower(field.Name[0:1]) == field.Name[0:1] {
			continue
		}
		// determine the json name of the field
		name := strings.TrimSpace(field.Tag.Get("json"))
		if name == "" || strings.HasPrefix(name, ",") {
			name = field.Name
		} else {
			// strip out things like , omitempty
			parts := strings.Split(name, ",")
			name = parts[0]
		}

		parts := strings.Split(name, ",") // foo,omitempty => foo
		name = parts[0]
		if name == "-" {
			// honor json ignore tag
			continue
		}
		if field.Anonymous {
			s.resolveStruct(field.Type)
			schema := s.api.Schemas[name]
			for k, item := range schema.Properties {
				object[k] = item
			}
		} else {
			schema := s.resolveField(field.Type, &field.Tag)
			object[name] = schema
			if v := field.Tag.Get("required"); v == "true" {
				required = append(required, name)
			}
		}
	}
	name := s.MakeName(o)
	schema := oas.ObjectOf(object, required...)
	s.api.AddSchema(name, schema)
	return name, s.api.RefSchema(name)
}

func (s *Object) enrichSchema(tag *reflect.StructTag, schema *oas.Schema) {
	for _, sTag := range structTags {
		var val = tag.Get(sTag)
		if val == "" {
			continue
		}
		switch sTag {
		case "oas-validation":
			validation := s.validation(val)
			schema.SchemaValidation = *validation
		case "oas-example":
			schema.Example = s.schemaTypedExample(val, schema.Type)
		case "oas-desc":
			schema.Description = val
		case "oas-default":
			schema.Default = val
		case "oas-format":
			schema.Format = val
		case "oas-permissions":
			if val == "readOnly" {
				schema.ReadOnly = true
			} else if val == "writeOnly" {
				schema.WriteOnly = true
			}
		}
	}
}

func (s *Object) schemaTypedExample(val string, schemaType oas.Type) interface{} {
	switch schemaType {
	case oas.TypeInteger:
		if v, err := strconv.ParseInt(val, 10, 64); err == nil {
			return v
		}
	case oas.TypeNumber:
		if v, err := strconv.ParseFloat(val, 64); err == nil {
			return v
		}
	case oas.TypeBoolean:
		if v, err := strconv.ParseBool(val); err == nil {
			return v
		}
	}
	return val
}

func (s *Object) MakeName(t reflect.Type) string {
	name := t.Name()
	return strings.Replace(name, "-", "_", -1)
}
