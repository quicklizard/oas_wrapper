package oas_wrapper

import (
	"github.com/stretchr/testify/suite"
	"reflect"
	"testing"
	"time"
)

type SchemaTestSuite struct {
	suite.Suite
	api *Api
}

func (s *SchemaTestSuite) SetupTest() {
	s.api = New("1.0", "Demo API", "Demo API description")
}

func (s *SchemaTestSuite) TestTimeSchema() {
	obj := newObject(s.api.oas)
	timeObj := reflect.TypeOf(time.Time{})
	schema := obj.resolveField(timeObj, nil)
	b, _ := s.api.oas.Schemas["Time"].MarshalJSON()
	s.T().Log(string(b), schema.Type)
}

func TestSchemaTestSuite(t *testing.T) {
	suite.Run(t, new(SchemaTestSuite))
}
