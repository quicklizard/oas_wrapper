package oas_wrapper

import (
	"github.com/go-courier/oas"
	"strings"
)

type Endpoint struct {
	api      *oas.OpenAPI
	examples []map[string]string
	method   string
	op       *oas.Operation
	path     string
}

// newEndpoint create a new Endpoint object which describes an OpenAPI operation
// end returns it.
// newEndpoint takes a reference to OpenAPI definitions object, which is used when mounting the endpoint,
// an API operation name, HTTP method path, description, summary and tags, which are used to describe the operation.
func newEndpoint(api *oas.OpenAPI, name, method, path, desc, summary string, tags []string) *Endpoint {
	op := oas.NewOperation(name).WithDesc(desc).WithSummary(summary).WithTags(tags...)
	return &Endpoint{api: api, examples: make([]map[string]string, 0), method: method, op: op, path: path}
}

// Mount adds the Endpoint as an OpenAPI operation to the OpenAPI definitions object
func (e *Endpoint) Mount() {
	if len(e.examples) > 0 {
		e.op.AddExtension("x-code-samples", e.examples)
	}
	m := e.resolveHTTPMethod(e.method)
	e.api.AddOperation(m, e.path, e.op)
}

// QueryParameter takes a name, description, OpenAPI Schema and required bool flag, and adds a new
// Endpoint query-parameter definition to OpenAPI definitions object
func (e *Endpoint) QueryParameter(name, desc string, schema *oas.Schema, required bool) *Endpoint {
	param := oas.QueryParameter(name, schema, required).WithDesc(desc)
	e.op.AddParameter(param)
	return e
}

// HeaderParameter takes a name, description, OpenAPI Schema and required bool flag, and adds a new
// Endpoint header parameter definition to OpenAPI definitions object
func (e *Endpoint) HeaderParameter(name, desc string, schema *oas.Schema, required bool) *Endpoint {
	param := oas.HeaderParameter(name, schema, required).WithDesc(desc)
	e.op.AddParameter(param)
	return e
}

// PathParameter takes a name, description and OpenAPI Schema, and adds a new
// Endpoint path-parameter definition to OpenAPI definitions object
func (e *Endpoint) PathParameter(name, desc string, schema *oas.Schema) *Endpoint {
	param := oas.PathParameter(name, schema).WithDesc(desc)
	e.op.AddParameter(param)
	return e
}

// RequestBody takes a description string, required bool flag, content-type and request payload object,
// and adds an endpoint request body schema definitions to OpenAPI Schema
func (e *Endpoint) RequestBody(desc string, required bool, contentType string, requestPayload interface{}) *Endpoint {
	requestObject := newObject(e.api)
	name, requestSchema := requestObject.Define(requestPayload)
	body := oas.NewRequestBody(desc, required)
	body.AddContent(contentType, oas.NewMediaTypeWithSchema(requestSchema))
	e.api.AddRequestBody(name, body)
	e.op.SetRequestBody(e.api.RefRequestBody(name))
	return e
}

// Response takes description string, HTTP status code, content-type, response type object and default response bool flag,
// and adds a response schema definitions to OpenAPI Schema for the given HTTP status code in the endpoint
func (e *Endpoint) Response(desc string, status int, contentType string, responseType interface{}, isDefault bool) *Endpoint {
	resp := oas.NewResponse(desc)
	responseObject := newObject(e.api)
	name, responseSchema := responseObject.Define(responseType)
	resp.AddContent(contentType, oas.NewMediaTypeWithSchema(responseSchema))
	e.op.AddResponse(status, resp)
	if isDefault {
		e.op.SetDefaultResponse(e.api.RefResponse(name))
	}
	return e
}

// CodeExample takes a code example language, label, and source code and adds them
// to endpoint docs asx-code-samples extension
func (e *Endpoint) CodeExample(lang, label, source string) *Endpoint {
	example := map[string]string{
		"lang":   lang,
		"label":  label,
		"source": source,
	}
	e.examples = append(e.examples, example)
	return e
}

// resolveHTTPMethod takes an HTTP method as string and resolves it to the
// respective oas internal HTTP method
func (e *Endpoint) resolveHTTPMethod(method string) oas.HttpMethod {
	switch strings.ToLower(method) {
	case "get":
		return oas.GET
	case "post":
		return oas.POST
	case "put":
		return oas.PUT
	case "delete":
		return oas.DELETE
	case "options":
		return oas.OPTIONS
	case "head":
		return oas.HEAD
	case "patch":
		return oas.TRACE
	case "trace":
		return oas.TRACE
	default:
		return ""
	}
}
