module bitbucket.org/quicklizard/oas_wrapper

go 1.16

require (
	github.com/go-courier/oas v1.2.1
	github.com/stretchr/testify v1.7.0
)
