package oas_wrapper

import (
	"github.com/go-courier/oas"
	"github.com/stretchr/testify/suite"
	"net/http"
	"testing"
)

type EndpointTestSuite struct {
	suite.Suite
	api *Api
}

func (s *EndpointTestSuite) SetupTest() {
	s.api = New("1.0", "Demo API", "Demo API description")
}

func (s *EndpointTestSuite) TestQueryParameter() {
	desc := "list all pets"
	summary := "return a list of pets"
	path := "/pets"
	method := "GET"
	tags := []string{"catalog"}
	s.api = New("1.0", "Demo API", "Demo API description")
	ep := s.api.NewEndpoint("pets", method, path, desc, summary, tags)
	ep.QueryParameter("page", "result page number", oas.Integer(), true)
	s.Assert().Len(ep.op.Parameters, 1)
	s.Assert().Equal("page", ep.op.Parameters[0].Name)
	s.Assert().Equal("result page number", ep.op.Parameters[0].Description)
	s.Assert().Equal(oas.PositionQuery, ep.op.Parameters[0].In)
	s.Assert().True(ep.op.Parameters[0].Required)
	s.Assert().Equal(oas.TypeInteger, ep.op.Parameters[0].Schema.Type)
}

func (s *EndpointTestSuite) TestHeaderParameter() {
	desc := "list all pets"
	summary := "return a list of pets"
	path := "/pets"
	method := "GET"
	tags := []string{"catalog"}
	s.api = New("1.0", "Demo API", "Demo API description")
	ep := s.api.NewEndpoint("pets", method, path, desc, summary, tags)
	ep.HeaderParameter("key", "auth key", oas.String(), true)
	s.Assert().Len(ep.op.Parameters, 1)
	s.Assert().Equal("key", ep.op.Parameters[0].Name)
	s.Assert().Equal("auth key", ep.op.Parameters[0].Description)
	s.Assert().Equal(oas.PositionHeader, ep.op.Parameters[0].In)
	s.Assert().True(ep.op.Parameters[0].Required)
	s.Assert().Equal(oas.TypeString, ep.op.Parameters[0].Schema.Type)
}

func (s *EndpointTestSuite) TestPathParameter() {
	desc := "list all pets"
	summary := "return a list of pets"
	path := "/pets"
	method := "GET"
	tags := []string{"catalog"}
	s.api = New("1.0", "Demo API", "Demo API description")
	ep := s.api.NewEndpoint("pets", method, path, desc, summary, tags)
	ep.PathParameter("ID", "item ID", oas.String())
	s.Assert().Len(ep.op.Parameters, 1)
	s.Assert().Equal("ID", ep.op.Parameters[0].Name)
	s.Assert().Equal("item ID", ep.op.Parameters[0].Description)
	s.Assert().Equal(oas.PositionPath, ep.op.Parameters[0].In)
	s.Assert().Equal(oas.TypeString, ep.op.Parameters[0].Schema.Type)
}

func (s *EndpointTestSuite) TestRequestBody() {
	type Pet struct {
		Name string `json:"name"`
		Kind string `json:"kind"`
	}
	desc := "create new pet"
	summary := "creates a new pet"
	path := "/pets"
	method := "POST"
	tags := []string{"catalog"}
	s.api = New("1.0", "Demo API", "Demo API description")
	ep := s.api.NewEndpoint("pets", method, path, desc, summary, tags)
	ep.RequestBody("new pet", true, "application/json", Pet{})
	for _, body := range ep.api.RequestBodies {
		s.Assert().Equal("new pet", body.Description)
		s.Assert().Equal(true, body.Required)
		s.Assert().Contains(body.Content, "application/json")
		s.Assert().IsType(&oas.MediaType{}, body.Content["application/json"])
		break
	}
}

func (s *EndpointTestSuite) TestResponse() {
	type Pet struct {
		Name string `json:"name"`
		Kind string `json:"kind"`
	}
	desc := "list all pets"
	summary := "return a list of pets"
	path := "/pets"
	method := "GET"
	tags := []string{"catalog"}
	s.api = New("1.0", "Demo API", "Demo API description")
	ep := s.api.NewEndpoint("pets", method, path, desc, summary, tags)
	ep.Response("list of pets", http.StatusOK, "application/json", []Pet{}, true)
	s.Assert().Len(ep.op.Responses.Responses, 1)
	s.Assert().Contains(ep.op.Responses.Responses, http.StatusOK)
	resp := ep.op.Responses.Responses[http.StatusOK]
	s.Assert().Equal(ep.op.Responses.Default, resp)
	s.Assert().Equal("list of pets", resp.Description)
	s.Assert().Contains(resp.Content, "application/json")
	s.Assert().IsType(&oas.MediaType{}, resp.Content["application/json"])
}

func (s *EndpointTestSuite) TestCodeExample() {
	desc := "list all pets"
	summary := "return a list of pets"
	path := "/pets"
	method := "GET"
	tags := []string{"catalog"}
	s.api = New("1.0", "Demo API", "Demo API description")
	ep := s.api.NewEndpoint("pets", method, path, desc, summary, tags)
	source := `POST /pet HTTP/1.1
Host: example.org
Content-Type: application/json; charset=utf-8
Content-Length: 137

{
  "status": "ok",
  "extended": true,
  "results": [
	{"value": 0, "type": "int64"},
	{"value": 1.0e+3, "type": "decimal"}
  ]
}`
	ep.CodeExample("HTTP", "HTTP Request", source)
	ep.Mount()
	s.Assert().Len(ep.examples, 1)
	s.Assert().Contains(ep.op.Extensions, "x-code-samples")
}

func TestEndpointSuite(t *testing.T) {
	suite.Run(t, new(EndpointTestSuite))
}
