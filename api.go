package oas_wrapper

import (
	"encoding/json"
	"github.com/go-courier/oas"
)

type Api struct {
	endpoints []*Endpoint
	oas       *oas.OpenAPI
}

func New(version, title, desc string) *Api {
	openapi := oas.NewOpenAPI()
	openapi.Version = version
	openapi.Title = title
	openapi.Description = desc

	return &Api{
		endpoints: make([]*Endpoint, 0),
		oas:       openapi,
	}
}

// License adds a license object to API definitions and returns Api instance for method chaining
func (a *Api) License(license string) *Api {
	a.oas.License = &oas.License{
		LicenseObject: oas.LicenseObject{
			Name: license,
		},
	}
	return a
}

// Tags adds tags to API definitions and returns Api instance for method chaining
func (a *Api) Tags(tags []string) *Api {
	for _, t := range tags {
		a.oas.AddTag(oas.NewTag(t))
	}
	return a
}

// Server adds server URL to API definitions and returns Api instance for method chaining
func (a *Api) Server(url string) *Api {
	a.oas.AddServer(oas.NewServer(url))
	return a
}

// NewEndpoint creates a new Endpoint object that describes an API endpoint and returns it
func (a *Api) NewEndpoint(name, method, path, desc, summary string, tags []string) *Endpoint {
	e := newEndpoint(a.oas, name, method, path, desc, summary, tags)
	a.endpoints = append(a.endpoints, e)
	return e
}

// Mount iterates over all registered Endpoint definitions and adds them as an OpenAPI operation
// to the API definitions
func (a *Api) Mount() {
	for _, e := range a.endpoints {
		e.Mount()
	}
}

// JSON Returns the JSON string representation of the API definitions
func (a *Api) JSON() string {
	data, _ := json.MarshalIndent(a.oas, "\t", "\t")
	return string(data)
}
