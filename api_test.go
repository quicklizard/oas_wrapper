package oas_wrapper

import (
	"github.com/stretchr/testify/suite"
	"testing"
)

type ApiTestSuite struct {
	suite.Suite
	api *Api
}

func (s *ApiTestSuite) SetupTest() {
	s.api = New("1.0", "Demo API", "Demo API description")
}

func (s *ApiTestSuite) TestNewApi() {
	s.api.License("MIT").Server("https://api.dummy.com").Tags([]string{"catalog"})
	s.Assert().Equal("Demo API description", s.api.oas.Description)
	s.Assert().Equal("Demo API", s.api.oas.Title)
	s.Assert().Equal("1.0", s.api.oas.Version)
	s.Assert().Len(s.api.oas.Servers, 1)
	s.Assert().Equal("https://api.dummy.com", s.api.oas.Servers[0].URL)
	s.Assert().Equal("MIT", s.api.oas.License.LicenseObject.Name)
	s.Assert().Len(s.api.oas.Tags, 1)
}

func (s *ApiTestSuite) TestEndpoint() {
	desc := "list all pets"
	summary := "return a list of pets"
	path := "/pets"
	method := "GET"
	tags := []string{"catalog"}
	s.api.License("MIT").Server("https://api.dummy.com").Tags([]string{"catalog"})
	ep := s.api.NewEndpoint("pets", method, path, desc, summary, tags)
	s.Assert().Equal(desc, ep.op.Description)
	s.Assert().Equal(summary, ep.op.Summary)
	s.Assert().Len(ep.op.Tags, len(tags))
}

func TestApiSuite(t *testing.T) {
	suite.Run(t, new(ApiTestSuite))
}

//func TestApi_New(t *testing.T) {
//	api := New("1.0", "Demo API", "Demo API description")
//	api.Tags([]string{"catalog"}).License("MIT").Server("https://api.dummy.com")
//	t.Log(api.JSON())
//}
//
//func TestApi_NewEndpoint(t *testing.T) {
//	type Pet struct {
//		Name string `json:"name"`
//		Kind string `json:"kind"`
//	}
//	api := New("1.0", "Demo API", "Demo API description")
//	api.Tags([]string{"catalog"}).License("MIT").Server("https://api.dummy.com")
//	ep := api.NewEndpoint("pets", "GET", "/pets", "list all pets", "return a list of pets", []string{"catalog"})
//	ep.Parameter("page", "result page number", oas.Integer(), true)
//	ep.Parameter("perPage", "number of results per page", oas.Integer(), false)
//	ep.Response("successful response", http.StatusOK, "application/json", []Pet{}, true)
//	api.Mount()
//	t.Log(api.JSON())
//}
